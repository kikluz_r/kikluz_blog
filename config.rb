# Activate and configure extensions
# https://middlemanapp.com/advanced/configuration/#configuring-extensions
activate :livereload
activate :sprockets

# activate :directory_indexes
# extensions
require 'lib/extensions/permalink.rb'
activate :permalink
activate :syntax
set :markdown_engine, :kramdown

activate :autoprefixer do |prefix|
  prefix.browsers = "last 2 versions"
end

activate :blog do |blog|
  blog.prefix = "blog"
end


import_path File.expand_path('bower_components', app.root)

# Per-page layout changes
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false
# set article_layout file for blogging
page "blog/*", :layout => :article_layout

# github userpages deploy
# activate :deploy do |deploy|
#   deploy.deploy_method = :git
#   deploy.build_before = true
#   deploy.branch   = "master"
#   deploy.remote   = "git@github.com:kikluz/kikluz_blog.git"
# end           		 


# github project pages deploy
activate :deploy do |deploy|
  deploy.deploy_method = :git
  deploy.build_before = true # default: false
end


configure :build do
	set :relative_links, true
	activate :minify_css
	activate :minify_javascript
	activate :asset_hash
	
	
	# Relative assets needed to deploy to Github Pages
	activate :relative_assets
	# Name of the project where you working on
	set :site_url, "/kikluz_blog"
end

# -------------------------------------------------------------------
# For custom domains on github pages
# http://willschenk.com/building-sites-with-middleman/
# -------------------------------------------------------------------
# For custom domains on github pages
# page "CNAME", layout: false
# _vendor support for Sprockets
# after_configuration do
#   sprockets.append_path File.join "#{root}", 'source/assets/_vendor'
# end

# Ignore files/paths
ignore '.idea/*'





